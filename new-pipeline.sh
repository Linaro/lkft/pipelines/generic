#!/bin/bash

ROOT_DIR="$(dirname "$(readlink -e "$0")")"
CONF_FILE="${ROOT_DIR}/api-gitlab.conf"

help() {
  echo "$0 --git-project KERNEL_REPO_NAME --git-repo KERNEL_REPO_URL --git-ref KERNEL_BRANCH [--git-sha KERNEL_SHA] [--glproject GITLAB_PROJECT]"
  echo
  echo "Exempli gratia:"
  echo "  $0 \\"
  echo "    --git-project linux-stable-rc \\"
  echo "    --git-repo    https://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable-rc.git \\"
  echo "    --git-ref     linux-6.2.y \\"
  echo "    --git-sha     5f50ce97de71b5278626756de07d906a3a4882d0"
  echo
  echo "The GITLAB_PROJECT runs the LKFT Pipeline. Defaults to \`${PROJECT}'."
  echo
  echo "GITLAB_PIPELINE_TOKEN can be set in ${CONF_FILE}"
}

# Define the $GITLAB_PIPELINE_TOKEN
if [ -f "${CONF_FILE}" ]; then
  # shellcheck disable=SC1090 disable=SC1091
  source "${CONF_FILE}"
fi

if [ ! -v GITLAB_PIPELINE_TOKEN ]; then
  echo "ERROR: Variable GITLAB_PIPELINE_TOKEN is unset and is required."
  echo
  help
  exit 1
fi

CURL="curl --silent"

# Defaults
PROJECT=Linaro/lkft/pipelines/generic
KERNEL_REPO_NAME=
KERNEL_REPO_URL=
KERNEL_BRANCH=
KERNEL_SHA=

while true; do
  case "$1" in
    -h | --help)
      help
      exit 0
      ;;
    --git-repo)
      KERNEL_REPO_URL="$2"
      shift 2
      ;;
    --git-ref)
      KERNEL_BRANCH="$2"
      shift 2
      ;;
    --git-sha)
      KERNEL_SHA="$2"
      shift 2
      ;;
    --git-project)
      case "$2" in
        linux-stable-rc | linux-stable | linux-mainline | linux-next)
          KERNEL_REPO_NAME="$2"
          shift 2
          ;;
        *)
          echo "ERROR: --git-project can only be one of:"
          echo "  linux-stable-rc | linux-stable | linux-mainline | linux-next"
          exit 1
          ;;
      esac
      ;;
    --glproject)
      PROJECT="$2"
      shift 2
      ;;
    *)
      break
      ;;
  esac
done

project_id="${PROJECT//\//%2F}"
project_url="https://gitlab.com/api/v4/projects/${project_id}"

for var in KERNEL_REPO_URL KERNEL_BRANCH KERNEL_REPO_NAME; do
  if [ -z "${!var}" ]; then
    echo "ERROR: ${var} is undefined."
    help
    exit 1
  fi
done

# Resolve Git SHA
if [ -z "${KERNEL_SHA}" ]; then
  KERNEL_SHA="$(git ls-remote "${KERNEL_REPO_URL}" "refs/heads/${KERNEL_BRANCH}" | tail -n1 | awk '{print $1}')"
fi

# shellcheck disable=SC2068
${CURL[@]} \
  --request POST \
  --form "ref=master" \
  --form "token=${GITLAB_PIPELINE_TOKEN}" \
  --form "variables[KERNEL_REPO_NAME]=${KERNEL_REPO_NAME}" \
  --form "variables[KERNEL_BRANCH]=${KERNEL_BRANCH}" \
  --form "variables[KERNEL_SHA]=${KERNEL_SHA}" \
  --form "variables[KERNEL_REPO_URL]=${KERNEL_REPO_URL}" \
  --form "variables[QA_SERVER]=https://qa-reports.linaro.org" \
  --form "variables[QA_TEAM]=lkft" \
  "${project_url}/trigger/pipeline" > output.json

jq -r .web_url output.json
pipeline_id="$(jq -r .id output.json)"
mv output.json "pipeline-${pipeline_id}.json"
