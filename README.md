# Generic pipeline

## Usage
```
./new-pipeline.sh --git-project KERNEL_REPO_NAME --git-repo KERNEL_REPO_URL --git-ref KERNEL_BRANCH [--git-sha KERNEL_SHA] [--glproject GITLAB_PROJECT]

Exempli gratia:
  ./new-pipeline.sh \
    --git-project linux-stable-rc \
    --git-repo    https://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable-rc.git \
    --git-ref     linux-6.2.y \
    --git-sha     5f50ce97de71b5278626756de07d906a3a4882d0

The GITLAB_PROJECT runs the LKFT Pipeline. Defaults to `Linaro/lkft/pipelines/generic'.

GITLAB_PIPELINE_TOKEN can be set in $HERE/api-gitlab.conf
```

`--git-sha` is optional if `--git-ref` is provided; it will be resolved online
to the most recent SHA in that branch in that Git repository.

Another example:
```
$ ./new-pipeline.sh \
  --git-project linux-stable-rc \
  --git-repo    https://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable-rc.git \
  --git-ref     linux-6.3.y
https://gitlab.com/Linaro/lkft/pipelines/generic/-/pipelines/898531420
```

This will build the latest 6.3 SHA from the linux-stable-rc tree.

The output of that command will be an URL pointing to the Gitlab pipeline
for that configuration.

## Installation

Clone this repo.

Add a **pipeline token** here:
  https://gitlab.com/Linaro/lkft/pipelines/generic/-/settings/ci_cd

Then create an `api-gitlab.conf` file in the same directory with this in:
```
GITLAB_PIPELINE_TOKEN=mytoken-from-gitlab
```
